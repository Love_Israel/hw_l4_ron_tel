#include "OutStream.h"
#include "FileStream.h"
#include "OutStreamEncrypted.h"
#include "Logger.h"
#include <iostream>




int main(int argc, char **argv)
{
	OutStream out = OutStream();
	OutStreamEncrypted outE = OutStreamEncrypted(2);
	FileStream fOut = FileStream("test.txt"); //open a file

	out << "hello im doctor and im" << 70 << "years old" << endline; //test printing out

	fOut << "hello im doctor and im " << 70 <<"years old" << endline; //print to a file
	outE << "hello im doctor and im" << 70 << "years old" << endline; //print encrypted msg to screen
	
	Logger log = Logger("log_test.txt", true);
	Logger log2 = Logger("log_test.txt", true); //make 2 logs writing to same file
	//testing them out...
	log.print("test");
	log.print("is");
	log2.print("good");
	log.print("yay !");
	log2.print("...");
	system("pause");
	
	return 0;
}
