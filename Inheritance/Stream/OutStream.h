#pragma once

#include <iostream>

class OutStream
{
	protected:
		 FILE* file;

	public:
		OutStream();
		~OutStream();

		OutStream& operator<<(char *str);
		OutStream& operator<<(int num);
		OutStream& operator<<(char*(*pf)());
};

char* endline();