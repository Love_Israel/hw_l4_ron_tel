#include "Logger.h"
#include <iostream>


int Logger::_row_num = 0;
FileStream* Logger::fs = nullptr; //initializing fs with nullptr

Logger ::Logger (char* filename, bool logToScreen) 
{
	if (!fs) //if fs is nullptr (the file didnt open yet)
		fs = new FileStream(filename);
	_to_screen = logToScreen;
}

Logger ::~Logger () 
{
	
}

void Logger :: print (char* msg) 
{
	*fs << _row_num<< ". " << msg << "\n"; //printing to file
	if (_to_screen) 
		os << _row_num << ". " << msg << "\n"; //printing to screen
	_row_num++; //up row num by 1
	
}


