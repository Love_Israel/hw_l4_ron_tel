#pragma once

#include <iostream>
#include "OutStream.h"



class FileStream : public OutStream 
{
	public:
		FileStream(char* path);
		~FileStream();
};
