#include "OutStreamEncrypted.h"
#include <iostream>
#include <string.h>

OutStreamEncrypted ::OutStreamEncrypted (int offset) 
{
	this->_offset = offset;
}

OutStreamEncrypted ::~OutStreamEncrypted () 
{

}

OutStreamEncrypted& OutStreamEncrypted ::operator<<(char* str) 
{
	std::string strF = std::string (str);
	std::string tempStr = "";
	for (int i = 0 ;  i < strF.length() ; i++) 
	{
		if (str[i] >= 32 && str[i] <= 126) //if char in conditions
		{
			tempStr += char(int(strF[i]) + _offset);
			if (int(tempStr[i]) > 126)
				tempStr[i] = char(int(tempStr[i]) - 126 + 32);
			if (int(tempStr[i]) < 32)
				tempStr[i] = char(int(tempStr[i]) + 126 - 31);
		}
		else
			tempStr += strF[i];
	}
	printf ("%s" , tempStr.c_str());
	return *this;
}

OutStreamEncrypted& OutStreamEncrypted :: operator<<(int num) 
{
	printf("%d", num);
	return *this;
}

OutStreamEncrypted& OutStreamEncrypted :: operator<<(char*(*pf)()) 
{
	printf(pf());
	return *this;
}

