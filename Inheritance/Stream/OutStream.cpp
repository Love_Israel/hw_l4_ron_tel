#include "OutStream.h"
#include <stdio.h>

OutStream::OutStream()
{
	file = stdout; //initializing file to be stdout
}

OutStream::~OutStream()
{
}

OutStream& OutStream::operator<<(char *str)
{
	fprintf(file , "%s", str);
	return *this;
}

OutStream& OutStream::operator<<(int num)
{
	fprintf(file ,"%d", num);
	return *this;
}

OutStream& OutStream::operator<<(char*(*pf)())
{
	fprintf (file ,pf());
	return *this;
}


char* endline()
{
	return "\n"; //returning \n(cant print to file beacose func dont member)
}
